Function Get-FreeNASDataset {

<#
.SYNOPSIS
Calls the FreeNAS API for a list of datasets and their statistics.
.DESCRIPTION
Data returned as a PSCustomObject with the following data points: 

.LINK 
https://bitbucket.org/natestovall/freenas-tools
.PARAMETER COMPUTERNAME
The hostname of the FreeNAS to connect to. 
.PARAMETER CREDENTIAL 
This parameter specifies a PSCredential object to be used for authentication.
.NOTES
	File Name:      Get-FreeNASVolume.ps1
	Version:        1.0
	Author:         Nate Stovall (natestovall _at_ gmail.com)
	Creation Date:  11/07/17
	Purpose/Change: Wanted to schedule a jenkins build to check how much free space I had left, and notify me when low.
	Future Improvements:
		* Unorthodox port support
		* Username/password parameters for noobs who don't know what a PSCredentialObject is :p
		* Switch to PowerShell Classes (PoSH 5+ only)
		* Allow * for VolumeName - call Get-FreeNASVolume
.EXAMPLE
Get-FreeNASVolume -ComputerName Erebor.middle-earth.local -Credential $MyCred
#>
#========================================================================================================================= PARAMETERS =====
	[CmdletBinding()]
	Param(
		[Parameter(Mandatory=$TRUE, ValueFromPipeline=$TRUE, ValueFromPipelineByPropertyName=$TRUE)] [Alias("host","hostname","__Server","CN","h","Name","Server")][String[]] $ComputerName,
		[Parameter(Mandatory=$FALSE)] [Alias("id")] [Int] $VolumeID=$NULL,
		[Parameter(Mandatory=$FALSE)] [Alias("volume","v")] [String] $VolumeName="",
		[Parameter(Mandatory=$TRUE)] [Alias("Cred","c")] [System.Management.Automation.PSCredential] $Credential
	)
	Begin {  #================================================================================================================= BEGIN =====
		Write-Verbose "$(Get-Date -DisplayHint DateTime)   $($MyInvocation.MyCommand.Name) started execution."
		#===== INITILIZATION =====
		$Data = $Output = @()
		$CleanName = $URL = $Head = $API = $NULL
		$APIPrefix = '/api/v1.0/storage/volume/'  #/api/v1.0/storage/volume/(int:id|string:name)/datasets/
		$APISuffix = '/datasets/'
		#==== PARAMETER CLEAN-UP =====
		If ( ($VolumeID -ne $NULL) -and ($VolumeName.length -gt 0) ){				#Parameter checking for VolumeID and VolumeName
			Throw "Please specify either a VolumeID or a VolumeName. Not both."
		} ElseIf ( ($VolumeID -eq $NULL) -and ($VolumeName.length -eq 0) ) {
			Throw "A VolumeID or a VolumeName must be specified."
		} #END If
		If ($VolumeID -ne $NULL) { $API = $APIPrefix + $VolumeID + $APISuffix }
		Else {$API = $APIPrefix + $VolumeName + $APISuffix }
	} #END Begin
	Process {  #============================================================================================================= PROCESS =====
		#==== PARAMETER CLEAN-UP =====
		Try {$CleanName = ([Net.Dns]::GetHostEntry($ComputerName)).HostName }
		Catch { 
			$CleanName = $NULL
			Write-Warning "Unable to resolve hostname `"$($ComputerName)`""
		} #END Catch
		If ($CleanName) {  # if $CleanName defined, it is a valid FQDN and in DNS
			$URL = 'https://' + $CleanName
			Try { $Head = Invoke-WebRequest $URL -DisableKeepAlive -UseBasicParsing -Method head -ErrorAction 'Stop' } # test connecting to https
			Catch {
				$URL = 'http://' + $CleanName # revert to http...
				Write-Warning "`"$($CleanName)`" is unsecured! You should configure SSL/TLS."
			} #END Catch
			$URL = $URL + $API
			Try { $Output = (Invoke-RestMethod -Method Get -Uri $URL -Credential $Credential -ErrorAction 'Stop')}
			Catch { $Output =  $NULL; Write-Warning "Unable to connect to FreeNAS API on `"$($URL)`"" }
			If ($Data) { $Output += $Data }
		}#END If $CleanName
	} #END Process
	End {  #===================================================================================================================== END =====
		Write-Output $Output
		Write-Verbose "$(Get-Date -DisplayHint DateTime)   $($MyInvocation.MyCommand.Name) finished execution."
	} #END End
} #END Function Get-FreeNASDataset
#============================================================================================================================== NOTES =====