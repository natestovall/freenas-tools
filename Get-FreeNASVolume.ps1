Function Get-FreeNASVolume {

<#
.SYNOPSIS
Calls the FreeNAS API for a list of volumes and their statistics.
.DESCRIPTION
Data returned as a PSCustomObject with the following data points: 
avail, children, id, is_decrypted, is_upgraded, mountpoint, name, status, used, used_pct, vol_encrypt, vol_encryptkey, vol_fstype, vol_guid, vol_name
.LINK 
https://bitbucket.org/natestovall/freenas-tools
.PARAMETER COMPUTERNAME
The hostname of the FreeNAS to connect to. 
.PARAMETER PORT
To specify a non-standard port the FreeNAS management website is listening on.
.PARAMETER CREDENTIAL 
This parameter specifies a PSCredential object to be used for authentication.
.NOTES
	File Name:      Get-FreeNASVolume.ps1
	Version:        1.0
	Author:         Nate Stovall (natestovall _at_ gmail.com)
	Creation Date:  11/07/17
	Purpose/Change: Wanted to schedule a jenkins build to check how much free space I had left, and notify me when low.
	Future Improvements:
		* Unorthodox port support
		* Username/password parameters for noobs who don't know what a PSCredentialObject is :p
		* Switch to PowerShell Classes (PoSH 5+ only)
.EXAMPLE
Get-FreeNASVolume -ComputerName Erebor.middle-earth.local -credential $MyCred
#>
#========================================================================================================================= PARAMETERS =====
	[CmdletBinding()]
	Param(
		[Parameter(Mandatory=$TRUE, ValueFromPipeline=$TRUE, ValueFromPipelineByPropertyName=$TRUE)] [Alias("host","hostname","__Server","CN","h","Name","Server")][String[]] $ComputerName,
		#[Parameter(Mandatory=$FALSE)] [Int] $Port,
		[Parameter(Mandatory=$TRUE)] [Alias("Cred","c")] [System.Management.Automation.PSCredential] $Credential
	)
	Begin {  #================================================================================================================= BEGIN =====
		Write-Verbose "$(Get-Date -DisplayHint DateTime)   $($MyInvocation.MyCommand.Name) started execution."
		#===== INITILIZATION =====
		$Data = $Output = @()
		$CleanName = $URL = $Head = $NULL
		$API = '/api/v1.0/storage/volume/'
	} #END Begin
	Process {  #============================================================================================================= PROCESS =====
		#==== PARAMETER CLEAN-UP =====
		Try {$CleanName = ([Net.Dns]::GetHostEntry($ComputerName)).HostName }
		Catch { 
			$CleanName = $NULL
			Write-Warning "Unable to resolve hostname `"$($ComputerName)`""
		} #END Catch
		If ($CleanName) {  # if $CleanName defined, it is a valid FQDN and in DNS
			$URL = 'https://' + $CleanName
			Try { $Head = Invoke-WebRequest $URL -DisableKeepAlive -UseBasicParsing -Method head -ErrorAction 'Stop' } # test connecting to https
			Catch {
				$URL = 'http://' + $CleanName # revert to http...
				Write-Warning "`"$($CleanName)`" is unsecured! You should configure SSL/TLS."
			} #END Catch
			$URL = $URL + $API
			Try { $Output = (Invoke-RestMethod -Method Get -Uri $URL -Credential $Credential -ErrorAction 'Stop')}
			Catch { $Output =  $NULL; Write-Warning "Unable to connect to FreeNAS API on `"$($URL)`"" }
			If ($Data) { $Output += $Data }
		}#END If $CleanName
	} #END Process
	End {  #===================================================================================================================== END =====
		Write-Output $Output
		Write-Verbose "$(Get-Date -DisplayHint DateTime)   $($MyInvocation.MyCommand.Name) finished execution."
	} #END End
#========================================================================================================================== FUNCTIONS =====
#========================================================================================================================== VARIABLES =====
#====================================================================================================================== INITILIZATION =====
#=============================================================================================================================== MAIN =====
#============================================================================================================================== BEGIN =====
#============================================================================================================================ PROCESS =====
#================================================================================================================================ END =====
#======================================================================================================================== TERMINATION =====
} #END Function Get-FreeNASVolume
#============================================================================================================================== NOTES =====
#[Parameter(Mandatory=$FALSE, ValueFromPipeline=$TRUE, ValueFromPipelineByPropertyName=$TRUE)] [Alias("VMHost","host","hostname","__Server","CN","h","Name")][String[]] $ServerList,