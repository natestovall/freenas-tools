Function Get-FreeNASVolume {

<#
.SYNOPSIS
Calls the FreeNAS API for a list of volumes and their statistics.
.DESCRIPTION
Data returned as a PSCustomObject with the following data points: 
avail, children, id, is_decrypted, is_upgraded, mountpoint, name, status, used, used_pct, vol_encrypt, vol_encryptkey, vol_fstype, vol_guid, vol_name
.LINK 
https://bitbucket.org/natestovall/freenas-tools
.PARAMETER COMPUTERNAME
The hostname of the FreeNAS to connect to. 
.PARAMETER PORT
To specify a non-standard port the FreeNAS management website is listening on.
.PARAMETER CREDENTIAL 
This parameter specifies a PSCredential object to be used for authentication.
.NOTES
	File Name:      .ps1
	Version:        1.0
	Author:         Nate Stovall (natestovall _at_ gmail.com)
	Creation Date:  11/07/17
	Purpose/Change: Wanted to schedule a jenkins build to check how much free space I had left, and notify me when low.
	Future Improvements:
		* 
.EXAMPLE
Get-FreeNASVolume -ComputerName Erebor.middle-earth.local -credential $MyCred
#>
#========================================================================================================================= PARAMETERS =====
	[CmdletBinding()]
	Param(
		[Parameter(Mandatory=$FALSE, ValueFromPipeline=$TRUE)] [Alias("host","hostname","__Server","CN","h","Name","Server")][String] $ComputerName,
		[Parameter(Mandatory=$FALSE)] [Alias("Cred","c")] [System.Management.Automation.PSCredential]$Credential
	)
	Begin {  #================================================================================================================= BEGIN =====
		Write-Verbose "$(Get-Date -DisplayHint DateTime)   $($MyInvocation.MyCommand.Name) started execution."
		#===== INITILIZATION =====
		$Output = @()
		$URL = $Head = $Data = $NULL
		$API = '/api/v1.0/storage/volume/'
		#==== PARAMETER CLEAN-UP =====
		If ( (($ComputerName.ToCharArray() | ?{$_ -eq '.'} | Measure-Object).Count) -eq 0 ) { #Test if $ComputerName is a FQDN of not
			Try {$ComputerName = ([Net.Dns]::GetHostEntry($ComputerName)).HostName }
			Catch { Throw "Unable to resolve hostname `"$($ComputerName)`"" }
		}
	} #END Begin
	Process {  #============================================================================================================= PROCESS =====
		$URL = 'https://' + $ComputerName
		Try { $Head = Invoke-WebRequest $URL -DisableKeepAlive -UseBasicParsing -Method head -ErrorAction 'Stop' } # test connecting to https
		Catch {
			$URL = 'http://' + $ComputerName # revert to http...
			Write-Warning ""
		} #END Catch
		$URL = $URL + $API
		Try { $Data = (Invoke-RestMethod -Method Get -Uri $URL -Credential $Credential -ErrorAction 'Stop')}
		Catch { $Data =  $NULL; Write-Warning "Unable to connect to FreeNAS API on `"$($URL)`"" }
		If ($Data) { $Output += $Data }
	} #END Process
	End {  #===================================================================================================================== END =====
		Write-Output $Output
		Write-Verbose "$(Get-Date -DisplayHint DateTime)   $($MyInvocation.MyCommand.Name) finished execution."
	} #END End
#========================================================================================================================== FUNCTIONS =====
#========================================================================================================================== VARIABLES =====
#====================================================================================================================== INITILIZATION =====
#=============================================================================================================================== MAIN =====
#============================================================================================================================== BEGIN =====
#============================================================================================================================ PROCESS =====
#================================================================================================================================ END =====
#======================================================================================================================== TERMINATION =====
} #END Function Get-FreeNASVolume
#============================================================================================================================== NOTES =====
#[Parameter(Mandatory=$FALSE, ValueFromPipeline=$TRUE, ValueFromPipelineByPropertyName=$TRUE)] [Alias("VMHost","host","hostname","__Server","CN","h","Name")][String[]] $ServerList,